all_b <- scan(file = "GSEA/associatedBcellGenes.txt",sep="\n",what="character")
#Assign names to mRNA
mrna_i <- mrna_i[colnames(penalty_matrix),]
#Get differentially expressed mRNA and miRNA
mrna_i_diff <- mrna_i[diff_expr_mrna,]
mirna_i_diff <- mirna_i[diff_expr_mirna,]
#Get differentially expressed mRNA and miRNA for the mean per time point
mrna_i_mean_diff <- mrna_i_mean[rownames(mrna_i) %in% diff_expr_mrna,]
mirna_i_mean_diff <- mirna_i_mean[diff_expr_mirna,]
#Get Correlation matrix
cor_mat <- get_cor(mrna_i,mirna_i)
#Get the data in triple/long format
triples_data <- matrix2long(t(cor_mat),new.ids=FALSE)
#Load the adjacency matrices
mirwalk_matrix <- read.csv("AdjMat/mirwalkMOut.txt",header=TRUE,sep=" ")
reptar_matrix <- read.table("AdjMat/reptarMOut.txt",header=TRUE,sep=" ")
targetscan_matrix <- read.csv("AdjMat/targetscanMOut.txt",header=TRUE,sep=" ")
pictar_matrix <- read.csv("AdjMat/pictarMOut.txt",header=TRUE,sep=" ")
connecttab_matrix <- read.csv("AdjMat/connectiontableMOut.txt",header=TRUE,sep=" ")
mirecords_matrix <- read.table("AdjMat/mirecordsMOut.txt",header=TRUE,sep=" ")
mirtarbase_matrix <- read.table("AdjMat/mirtarbaseMOut.txt",header=TRUE,sep=" ")
#Everything above 1 is just 1, this is caused by multiple binding spots on mRNA
mirwalk_matrix[mirwalk_matrix > 1] <- 1
reptar_matrix[reptar_matrix > 1] <- 1
targetscan_matrix[targetscan_matrix > 1] <- 1
pictar_matrix[pictar_matrix > 1] <- 1
connecttab_matrix[connecttab_matrix > 1] <- 1
mirecords_matrix[mirecords_matrix > 1] <- 1
mirtarbase_matrix[mirtarbase_matrix > 1] <- 1
#Make a union of target databases
preds <- mirwalk_matrix+reptar_matrix+targetscan_matrix+pictar_matrix+connecttab_matrix
#Define intersections with at least X predictions
preds[preds < 2] <- 0
two <- preds
two[two > 1] <- 1
preds[preds < 3] <- 0
three <- preds
three[three > 1] <- 1
preds[preds < 4] <- 0
four <- preds
four[four > 1] <- 1
preds[preds < 5] <- 0
five <- preds
five[five > 1] <- 1
if (writeToPDF) {
pdf(file="Figs/Figure5.pdf",width=7.5,height=7.5)
}
boxplot(data.frame(auc,rep(0.61,nrow(auc)),rep(0.59,nrow(auc)),auc_random),ylim=c(0,1),names=c("Evidence codes","TargetScan","RepTar","Random response"),ylab="AUC")
abline(h=0.5,lty=2,col="grey")
boxplot(data.frame(auc,rep(0.61,nrow(auc)),rep(0.59,nrow(auc)),auc_random),ylim=c(0,1),names=c("Evidence codes","TargetScan","RepTar","Random response"),ylab="AUC",add=T)
if (writeToPDF) {
dev.off()
}
par(mfrow=c(1,1))
#Define colours for lines
cols <- palette(rainbow(9))
#Get correlation matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
triples_data_diff <- matrix2long(t(cor_mat),new.ids=FALSE)
triples_data_diff <- triples_data_diff[triples_data_diff$actor.id %in% diff_expr_mirna,]
triples_data_diff <- triples_data_diff[triples_data_diff$partner.id %in% diff_expr_mrna,]
triples_data_diff$edges <- paste(as.vector(triples_data_diff[,2]),as.vector(triples_data_diff[,1]),sep="+")
par(mfrow=c(1,1))
if (writeToPDF) {
pdf(file="Figs/TabasComparisonFig.pdf",width=8.5,height=6.0)
}
#Make the plot, this is done by getting the rank of interesting (B-cell) mRNAs and plotting it per target database of intersections thereof
par(mar=c(4.5,4.5,2,14.5), xpd=TRUE)
plot(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70),xlab="Top genes",ylab="Gene(s) associated with B-cell terms")
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70))
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b),type="l",col=cols[2],lty=1,pch=2,cex=1.0)
points(seq(20,1000,200),(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b))[seq(20,1000,200)],pch=2,col=cols[2])
lines(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b),type="l",col=cols[3],lty=1,pch=3,cex=1.0)
points(seq(30,1000,200),(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b))[seq(30,1000,200)],pch=3,col=cols[3])
lines(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b),type="l",col=cols[4],lty=1,pch=4,cex=1.0)
points(seq(40,1000,200),(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b))[seq(40,1000,200)],pch=4,col=cols[4])
#lines(get_rank_gsea(get_gsea_set(two)[1:1000],all_b),col=cols[5],type="l",lty=1,pch=5,cex=1.0)
#points(seq(50,1000,200),(get_rank_gsea(get_gsea_set(two)[1:1000],all_b))[seq(50,1000,200)],pch=5,col=cols[5])
#
#lines(get_rank_gsea(get_gsea_set(three)[1:1000],all_b),col=cols[6],type="l",lty=1,pch=6,cex=1.0)
#points(seq(60,1000,200),(get_rank_gsea(get_gsea_set(three)[1:1000],all_b))[seq(60,1000,200)],pch=6,col=cols[6])
#
#lines(get_rank_gsea(get_gsea_set(four)[1:1000],all_b),col=cols[7],type="l",lty=1,pch=7,cex=1.0)
#points(seq(70,1000,200),(get_rank_gsea(get_gsea_set(four)[1:1000],all_b))[seq(70,1000,200)],pch=7,col=cols[7])
#
lines(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b),type="l",col=cols[5],lty=1,pch=5,cex=1.0)
points(seq(80,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b))[seq(80,1000,200)],pch=5,col=cols[5])
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix,lambda=1.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="blue",lty=1,pch=6,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=6,col="blue")
cols[8] <- "blue"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=1.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="black",lty=1,pch=9,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=9,col="black")
cols[8] <- "black"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="grey",lty=1,pch=8,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=8,col="grey")
cols[9] <- "grey"
legend("right",inset=c(-0.5),c("miRWalk","RepTar","TargetScan",
"picTar", #"Two","Three","Four",
"No filtering",expression(paste("Tabas penalty (",lambda,"=1.0)")),
expression(paste("PLR penalty (",lambda,"=1.0)")),expression(paste("PLR penalty (",lambda,"=2.0)"))),
col= c(cols[1:5],c("blue","black","grey")), lty = c(rep(1,9)),pch=c(c(1:5),c(6,9,8)),fill=c(rep(NA,9)),border=c(rep("white",9)))
if (writeToPDF) {
dev.off()
}
par(mfrow=c(1,1))
#Define colours for lines
cols <- palette(rainbow(9))
#Get correlation matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
triples_data_diff <- matrix2long(t(cor_mat),new.ids=FALSE)
triples_data_diff <- triples_data_diff[triples_data_diff$actor.id %in% diff_expr_mirna,]
triples_data_diff <- triples_data_diff[triples_data_diff$partner.id %in% diff_expr_mrna,]
triples_data_diff$edges <- paste(as.vector(triples_data_diff[,2]),as.vector(triples_data_diff[,1]),sep="+")
par(mfrow=c(1,1))
if (writeToPDF) {
pdf(file="Figs/TheComparisonFig.pdf",width=8.5,height=6.0)
}
#Make the plot, this is done by getting the rank of interesting (B-cell) mRNAs and plotting it per target database of intersections thereof
par(mar=c(4.5,4.5,2,14.5), xpd=TRUE)
plot(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70),xlab="Top genes",ylab="Gene(s) associated with B-cell terms")
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70))
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b),type="l",col=cols[2],lty=1,pch=2,cex=1.0)
points(seq(20,1000,200),(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b))[seq(20,1000,200)],pch=2,col=cols[2])
lines(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b),type="l",col=cols[3],lty=1,pch=3,cex=1.0)
points(seq(30,1000,200),(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b))[seq(30,1000,200)],pch=3,col=cols[3])
lines(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b),type="l",col=cols[4],lty=1,pch=4,cex=1.0)
points(seq(40,1000,200),(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b))[seq(40,1000,200)],pch=4,col=cols[4])
lines(get_rank_gsea(get_gsea_set(two)[1:1000],all_b),col=cols[5],type="l",lty=1,pch=5,cex=1.0)
points(seq(50,1000,200),(get_rank_gsea(get_gsea_set(two)[1:1000],all_b))[seq(50,1000,200)],pch=5,col=cols[5])
lines(get_rank_gsea(get_gsea_set(three)[1:1000],all_b),col=cols[6],type="l",lty=1,pch=6,cex=1.0)
points(seq(60,1000,200),(get_rank_gsea(get_gsea_set(three)[1:1000],all_b))[seq(60,1000,200)],pch=6,col=cols[6])
lines(get_rank_gsea(get_gsea_set(four)[1:1000],all_b),col=cols[7],type="l",lty=1,pch=7,cex=1.0)
points(seq(70,1000,200),(get_rank_gsea(get_gsea_set(four)[1:1000],all_b))[seq(70,1000,200)],pch=7,col=cols[7])
lines(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b),type="l",col=cols[8],lty=1,pch=8,cex=1.0)
points(seq(80,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b))[seq(80,1000,200)],pch=8,col=cols[8])
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=0.5,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="grey",lty=1,pch=11,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=11,col="grey")
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=1.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="black",lty=1,pch=9,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=9,col="black")
#cols[8] <- "black"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="grey",lty=1,pch=10,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=10,col="grey")
legend("right",inset=c(-0.5),c("miRWalk","RepTar","TargetScan",
"picTar", "Two","Three","Four",
"No filtering",expression(paste("PLR penalty (",lambda,"=0.5)")),
expression(paste("PLR penalty (",lambda,"=1.0)")),expression(paste("PLR penalty (",lambda,"=2.0)"))),
col= c(cols[1:8],c("grey","black","grey")), lty = c(rep(1,11)),pch=c(c(1:8),c(11,9,10)),fill=c(rep(NA,11)),border=c(rep("white",11)))
if (writeToPDF) {
dev.off()
}
expression
par(mfrow=c(1,1))
#Define colours for lines
cols <- palette(rainbow(9))
#Get correlation matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
triples_data_diff <- matrix2long(t(cor_mat),new.ids=FALSE)
triples_data_diff <- triples_data_diff[triples_data_diff$actor.id %in% diff_expr_mirna,]
triples_data_diff <- triples_data_diff[triples_data_diff$partner.id %in% diff_expr_mrna,]
triples_data_diff$edges <- paste(as.vector(triples_data_diff[,2]),as.vector(triples_data_diff[,1]),sep="+")
par(mfrow=c(1,1))
if (writeToPDF) {
pdf(file="Figs/TabasComparisonFig.pdf",width=8.5,height=6.0)
}
#Make the plot, this is done by getting the rank of interesting (B-cell) mRNAs and plotting it per target database of intersections thereof
par(mar=c(4.5,4.5,2,14.5), xpd=TRUE)
plot(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70),xlab="Top genes",ylab="Gene(s) associated with B-cell terms")
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70))
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b),type="l",col=cols[2],lty=1,pch=2,cex=1.0)
points(seq(20,1000,200),(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b))[seq(20,1000,200)],pch=2,col=cols[2])
lines(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b),type="l",col=cols[3],lty=1,pch=3,cex=1.0)
points(seq(30,1000,200),(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b))[seq(30,1000,200)],pch=3,col=cols[3])
lines(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b),type="l",col=cols[4],lty=1,pch=4,cex=1.0)
points(seq(40,1000,200),(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b))[seq(40,1000,200)],pch=4,col=cols[4])
#lines(get_rank_gsea(get_gsea_set(two)[1:1000],all_b),col=cols[5],type="l",lty=1,pch=5,cex=1.0)
#points(seq(50,1000,200),(get_rank_gsea(get_gsea_set(two)[1:1000],all_b))[seq(50,1000,200)],pch=5,col=cols[5])
#
#lines(get_rank_gsea(get_gsea_set(three)[1:1000],all_b),col=cols[6],type="l",lty=1,pch=6,cex=1.0)
#points(seq(60,1000,200),(get_rank_gsea(get_gsea_set(three)[1:1000],all_b))[seq(60,1000,200)],pch=6,col=cols[6])
#
#lines(get_rank_gsea(get_gsea_set(four)[1:1000],all_b),col=cols[7],type="l",lty=1,pch=7,cex=1.0)
#points(seq(70,1000,200),(get_rank_gsea(get_gsea_set(four)[1:1000],all_b))[seq(70,1000,200)],pch=7,col=cols[7])
#
lines(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b),type="l",col=cols[5],lty=1,pch=5,cex=1.0)
points(seq(80,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b))[seq(80,1000,200)],pch=5,col=cols[5])
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix,lambda=1.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="blue",lty=1,pch=6,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=6,col="blue")
cols[8] <- "blue"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
#penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=1.0,new_max_product=2.0)
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="black",lty=1,pch=9,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=9,col="black")
cols[8] <- "black"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="grey",lty=1,pch=8,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=8,col="grey")
cols[9] <- "grey"
legend("right",inset=c(-0.5),c("miRWalk","RepTar","TargetScan",
"picTar", #"Two","Three","Four",
"No filtering",expression(paste("Tabas penalty (",lambda,"=1.0)")),
expression(paste("PLR penalty (",lambda,"=1.0)")),expression(paste("PLR penalty (",lambda,"=2.0)"))),
col= c(cols[1:5],c("blue","black","grey")), lty = c(rep(1,9)),pch=c(c(1:5),c(6,9,8)),fill=c(rep(NA,9)),border=c(rep("white",9)))
if (writeToPDF) {
dev.off()
}
par(mfrow=c(1,1))
#Define colours for lines
cols <- palette(rainbow(9))
#Get correlation matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
triples_data_diff <- matrix2long(t(cor_mat),new.ids=FALSE)
triples_data_diff <- triples_data_diff[triples_data_diff$actor.id %in% diff_expr_mirna,]
triples_data_diff <- triples_data_diff[triples_data_diff$partner.id %in% diff_expr_mrna,]
triples_data_diff$edges <- paste(as.vector(triples_data_diff[,2]),as.vector(triples_data_diff[,1]),sep="+")
par(mfrow=c(1,1))
if (writeToPDF) {
pdf(file="Figs/TabasComparisonFig.pdf",width=8.5,height=6.0)
}
#Make the plot, this is done by getting the rank of interesting (B-cell) mRNAs and plotting it per target database of intersections thereof
par(mar=c(4.5,4.5,2,14.5), xpd=TRUE)
plot(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70),xlab="Top genes",ylab="Gene(s) associated with B-cell terms")
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70))
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b),type="l",col=cols[2],lty=1,pch=2,cex=1.0)
points(seq(20,1000,200),(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b))[seq(20,1000,200)],pch=2,col=cols[2])
lines(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b),type="l",col=cols[3],lty=1,pch=3,cex=1.0)
points(seq(30,1000,200),(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b))[seq(30,1000,200)],pch=3,col=cols[3])
lines(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b),type="l",col=cols[4],lty=1,pch=4,cex=1.0)
points(seq(40,1000,200),(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b))[seq(40,1000,200)],pch=4,col=cols[4])
#lines(get_rank_gsea(get_gsea_set(two)[1:1000],all_b),col=cols[5],type="l",lty=1,pch=5,cex=1.0)
#points(seq(50,1000,200),(get_rank_gsea(get_gsea_set(two)[1:1000],all_b))[seq(50,1000,200)],pch=5,col=cols[5])
#
#lines(get_rank_gsea(get_gsea_set(three)[1:1000],all_b),col=cols[6],type="l",lty=1,pch=6,cex=1.0)
#points(seq(60,1000,200),(get_rank_gsea(get_gsea_set(three)[1:1000],all_b))[seq(60,1000,200)],pch=6,col=cols[6])
#
#lines(get_rank_gsea(get_gsea_set(four)[1:1000],all_b),col=cols[7],type="l",lty=1,pch=7,cex=1.0)
#points(seq(70,1000,200),(get_rank_gsea(get_gsea_set(four)[1:1000],all_b))[seq(70,1000,200)],pch=7,col=cols[7])
#
lines(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b),type="l",col=cols[5],lty=1,pch=5,cex=1.0)
points(seq(80,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b))[seq(80,1000,200)],pch=5,col=cols[5])
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix,lambda=1.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="blue",lty=1,pch=6,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=6,col="blue")
cols[8] <- "blue"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
#penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=1.0,new_max_product=2.0)
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="black",lty=1,pch=9,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=9,col="black")
cols[8] <- "black"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="grey",lty=1,pch=8,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=8,col="grey")
cols[9] <- "grey"
legend("right",inset=c(-0.5),c("miRWalk","RepTar","TargetScan",
"picTar", #"Two","Three","Four",
"No filtering",expression(paste("Tabas penalty (",lambda,"=1.0)")),
expression(paste("PLR penalty (",lambda,"=1.0)")),expression(paste("PLR penalty (",lambda,"=2.0)"))),
col= c(cols[1:5],c("blue","black","grey")), lty = c(rep(1,9)),pch=c(c(1:5),c(6,9,8)),fill=c(rep(NA,9)),border=c(rep("white",9)))
if (writeToPDF) {
dev.off()
}
par(mfrow=c(1,1))
#Define colours for lines
cols <- palette(rainbow(9))
#Get correlation matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
triples_data_diff <- matrix2long(t(cor_mat),new.ids=FALSE)
triples_data_diff <- triples_data_diff[triples_data_diff$actor.id %in% diff_expr_mirna,]
triples_data_diff <- triples_data_diff[triples_data_diff$partner.id %in% diff_expr_mrna,]
triples_data_diff$edges <- paste(as.vector(triples_data_diff[,2]),as.vector(triples_data_diff[,1]),sep="+")
par(mfrow=c(1,1))
if (writeToPDF) {
pdf(file="Figs/TabasComparisonFig.pdf",width=8.5,height=6.0)
}
#Make the plot, this is done by getting the rank of interesting (B-cell) mRNAs and plotting it per target database of intersections thereof
par(mar=c(4.5,4.5,2,14.5), xpd=TRUE)
plot(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70),xlab="Top genes",ylab="Gene(s) associated with B-cell terms")
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(1:1000,get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b),type="l",col=cols[1],lty=1,pch=1,cex=1.0,ylim=c(0,70))
points(seq(10,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix)[1:1000],all_b))[seq(10,1000,200)],pch=1,col=cols[1])
lines(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b),type="l",col=cols[2],lty=1,pch=2,cex=1.0)
points(seq(20,1000,200),(get_rank_gsea(get_gsea_set(reptar_matrix)[1:1000],all_b))[seq(20,1000,200)],pch=2,col=cols[2])
lines(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b),type="l",col=cols[3],lty=1,pch=3,cex=1.0)
points(seq(30,1000,200),(get_rank_gsea(get_gsea_set(targetscan_matrix)[1:1000],all_b))[seq(30,1000,200)],pch=3,col=cols[3])
lines(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b),type="l",col=cols[4],lty=1,pch=4,cex=1.0)
points(seq(40,1000,200),(get_rank_gsea(get_gsea_set(pictar_matrix)[1:1000],all_b))[seq(40,1000,200)],pch=4,col=cols[4])
#lines(get_rank_gsea(get_gsea_set(two)[1:1000],all_b),col=cols[5],type="l",lty=1,pch=5,cex=1.0)
#points(seq(50,1000,200),(get_rank_gsea(get_gsea_set(two)[1:1000],all_b))[seq(50,1000,200)],pch=5,col=cols[5])
#
#lines(get_rank_gsea(get_gsea_set(three)[1:1000],all_b),col=cols[6],type="l",lty=1,pch=6,cex=1.0)
#points(seq(60,1000,200),(get_rank_gsea(get_gsea_set(three)[1:1000],all_b))[seq(60,1000,200)],pch=6,col=cols[6])
#
#lines(get_rank_gsea(get_gsea_set(four)[1:1000],all_b),col=cols[7],type="l",lty=1,pch=7,cex=1.0)
#points(seq(70,1000,200),(get_rank_gsea(get_gsea_set(four)[1:1000],all_b))[seq(70,1000,200)],pch=7,col=cols[7])
#
lines(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b),type="l",col=cols[5],lty=1,pch=5,cex=1.0)
points(seq(80,1000,200),(get_rank_gsea(get_gsea_set(mirwalk_matrix+1)[1:1000],all_b))[seq(80,1000,200)],pch=5,col=cols[5])
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="blue",lty=1,pch=6,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=6,col="blue")
cols[8] <- "blue"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
#penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=1.0,new_max_product=2.0)
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="black",lty=1,pch=9,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=9,col="black")
cols[8] <- "black"
#Transform correlations to fisher-z values so they can be shrinked using the penalty matrix
cor_mat <- get_cor(mrna_i_mean,mirna_i_mean)
cor_mat <- fisherz(cor_mat)
#Get the scaled penalty matrix
penalty_matrix_scal <- get_penalty_matrix(penalty_matrix_PLR,lambda=2.0,new_max_product=2.0)
#Get a penalized correlation matrix
cor_mat_pen <- get_pen_cor(cor_mat,penalty_matrix_scal)
#Get differentially expressed miRNAs and mRNAs
cor_mat_pen_diff <- cor_mat_pen[rownames(cor_mat_pen) %in% diff_expr_mirna,colnames(cor_mat_pen) %in% diff_expr_mrna]
#Get rankings of mRNA identifiers
pen_trip <- matrix2long(cor_mat_pen_diff,new.ids=FALSE)
retr <- get_gsea_set(mirwalk_matrix+1,trip_data=pen_trip)[1:1000]
#Plot penalized version
lines(get_rank_gsea(retr,all_b),col="grey",lty=1,pch=8,cex=1.0,type="l")
points(seq(90,1000,200),(get_rank_gsea(retr,all_b))[seq(90,1000,200)],pch=8,col="grey")
cols[9] <- "grey"
legend("right",inset=c(-0.5),c("miRWalk","RepTar","TargetScan",
"picTar", #"Two","Three","Four",
"No filtering",expression(paste("Tabas penalty (",lambda,"=1.0)")),
expression(paste("PLR penalty (",lambda,"=1.0)")),expression(paste("PLR penalty (",lambda,"=2.0)"))),
col= c(cols[1:5],c("blue","black","grey")), lty = c(rep(1,9)),pch=c(c(1:5),c(6,9,8)),fill=c(rep(NA,9)),border=c(rep("white",9)))
if (writeToPDF) {
dev.off()
}
