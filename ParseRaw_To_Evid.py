#Author:            Robbin Bouwmeester (supervisors: Johan Westerhuis and Frans van der Kloet)
#Purpose:           Combining the evidence codes of multiple files
#Date:              18-09-2015
#Python version:    2.7.5
#Bugs:              No bugs found...

import sys
import zipfile

"""Class that can contain information about miRNAs
        Inputs:
            identifier - set the identifier of a miRNA, for example "mmu-miR-1a"
        Outputs:
		 -
"""
class miRNAObj(object):
	#Instantiate the miRNA object, a identifier needs to be supplied
	def __init__(self, identifier):
		self.full_id = identifier
		self.mature = ""
		self.number = ""
		self.isoform = ""
		self.organism = ""
		self.transcript_pos = ""
		self.side = ""
		self.create_mult_id()
	
	#Analyze the identifier that was supplied in the beginning
	def create_mult_id(self):
		#Split the identifier
		split_id = self.full_id.split("-")
		#If an identifier is longer than 1...
		if len(split_id) > 1:
			#Set the organism
			self.organism = split_id[0]
			#Is the miRNA mature?
			self.mature = split_id[1]
			
			#Only a miRNA family number or also an isoform?
			if split_id[2][-1].isdigit():
				self.number = split_id[2]
			else:
				self.number = split_id[2][:-1]
				self.isoform = split_id[2][-1]
		#What is the position of the miRNA? 3p or 5p?
		if len(split_id) > 3:
			if split_id[3].isdigit():
				self.transcript_pos = split_id[3]
			else:
				self.side = split_id[3]
	
	#####################################################################
	# Retrieving identifiers in different forms and shapes, if possible #
	#####################################################################
	def get_simple_id(self):
		return "%s-%s-%s" % (self.organism,self.mature,self.number)
	
	def get_id_isoform(self):
		if self.isoform == "": return False
		return "%s-%s-%s%s" % (self.organism,self.mature,self.number,self.isoform)
	
	def get_id_pos(self):
		if self.side == "": return False
		return "%s-%s-%s-%s" % (self.organism,self.mature,self.number,self.side)
	
	def get_id_isoform_pos(self):
		if self.isoform == "" or self.side == "": return False
		return "%s-%s-%s%s-%s" % (self.organism,self.mature,self.number,self.isoform,self.side)
	
	def get_id_isoform_pos_transc(self):
		if self.isoform == "" or self.transcript_pos == "" or self.side == "": return False
		return "%s-%s-%s%s-%s-%s" % (self.organism,self.mature,self.number,self.isoform,self.transcript_pos,self.side)
	#####################################################################

"""Function that combines the evidence codes of miRNAs
	- deprecated -
"""	
def combine_evid_mirnaid(evid,mirna_id):
	if evid.split("-")[-1].startswith("s"):
		return evid.split("-")[0:-1]+"-"+mirna_id+"-"+evid.split("-")[-1]
	else: return evid+"-"+mirna_id

"""Function that writes a matrix to a file
        Inputs:
            matrix - two-dimensional list
			mRNAs - mRNA identifiers
			miRNAs - miRNA identifiers
			write_row_column_id - should the identifiers of miRNAs and mRNAs be written?
        Outputs:
		 -	
"""
def write_matrix(matrix,mRNAs=[],miRNAs=[],write_row_column_id=False,outfile_name="outTesting.txt"):
	outfile = open(outfile_name,"w")
	#Do we need to write a header?
	if write_row_column_id:	outfile.write("mRNA_identifier\t%s\n" % "\t".join(miRNAs))
	
	#Iterate over the rows
	for row_index in range(len(matrix)):
		row = matrix[row_index]
		
		#If we need to write the row identifiers... Do so.
		if write_row_column_id:	id = mRNAs[row_index]
		row = map(str, row)
		#Write values ad identifier if needed
		if write_row_column_id:	outfile.write("%s\t%s\n" % (id,"\t".join(row)))
		else: outfile.write("%s\n" % ("\t".join(row)))
	outfile.close()

"""Function that writes a matrix to a file
        Inputs:
            matrix - two-dimensional list
			mRNAs - mRNA identifiers
			miRNAs - miRNA identifiers
        Outputs:
		 -	
"""
def write_venn(matrix,mRNAs,miRNAs,outfile_name="outTesting.txt"):
	outfile = open(outfile_name,"w")
	for row_index in range(len(matrix)):
		row = matrix[row_index]
		for column_index in range(len(row)):
			if matrix[row_index][column_index] == 1:
				mRNA = mRNAs[row_index]
				miRNA = miRNAs[column_index]
				outfile.write("%s-%s\n" % (mRNA,miRNA))
	outfile.close()

"""Function that writes the matrix in triple format so it can be used in cytoscape
        Inputs:
            matrix - two-dimensional list
			mRNAs - mRNA identifiers
			miRNAs - miRNA identifiers
        Outputs:
		 -	
"""	
def write_graph(matrix,mRNAs,miRNAs,tool="",outfile_name="cytoscape.txt"):
	outfile = open(outfile_name,"a")
	for row_index in range(len(matrix)):
		row = matrix[row_index]
		for column_index in range(len(row)):
			if matrix[row_index][column_index] == 1:
				mRNA = mRNAs[row_index]
				miRNA = miRNAs[column_index]
				outfile.write("%s\t%s\t%s\n" % (mRNA,miRNA,tool))
	outfile.close()

#######################################################
# The following functions all do the same. They parse #
# a target database and fill in an associated evidence#
# code. The steps taken in these functions are very   #
# specific to the used target database...			  #
#######################################################

def parse_connection_table(infile_name,outfile_name="TargetDatabases/ConnectionTable.txt"):
	infile = open(infile_name)
	outfile = open(outfile_name,"w")
	for line in infile:
		line = line.strip()
		evid_code = 50*[0]
		evid_code[7] = 1
		evid_code[9] = 1
		
		outfile.write("%s\t%s\n" % (line,"".join(map(str, evid_code))))
	outfile.close()

def parse_mirwalk(infile_name,outfile_name="TargetDatabases/miRWalk.txt"):
	zip = zipfile.ZipFile(infile_name)
	infile = zip.read("miRWalk.txt")
	outfile = open(outfile_name,"w")
	for line in infile.split("\n"):
		print line
		evid_code = 50*[0]
		line = line.strip()
		
		evid_code[6] = 1
		evid_code[9] = 1
		
		outfile.write("%s\t%s\n" % (line,"".join(map(str, evid_code))))
	outfile.close()
	
def parse_reptar(infile_name,outfile_name="TargetDatabases/reptar.txt"):
	zip = zipfile.ZipFile(infile_name)
	infile = zip.read("mouse_pred-RepTar.txt")
	outfile = open(outfile_name,"w")
	for line in infile.split("\n"):
		if len(line) == 0: continue
		evid_code = 50*[0]
		line = line.strip().split("\t")
		identifier = line[0].split(":::")[1]
		miRNA_id = line[1]
		try: score = float(line[5].replace("nm:",""))
		except: continue
		
		if score < 0.364: evid_code[35] = 1
		if (score >= 0.364) and (score < 0.451): evid_code[36] = 1
		if score > 0.451: evid_code[37] = 1
		
		evid_code[4] = 1
		evid_code[9] = 1
		
		outfile.write("%s\t%s\t%s\n" % (identifier,miRNA_id,"".join(map(str, evid_code))))
	outfile.close()

def parse_targetscan(infile_name,outfile_name="TargetDatabases/targetscan.txt"):
	infile = open(infile_name)
	outfile = open(outfile_name,"w")
	for line in infile:
		evid_code = 50*[0]
		line = line.strip().split("\t")
		id = line[0]
		miRNA_id = line[12]
		try: score = float(line[13])
		except: continue

		if score > -0.069: evid_code[32] = 1
		if (score <= -0.069) and (score > -0.157): evid_code[33] = 1
		if score < -0.157: evid_code[34] = 1
		
		evid_code[2] = 1
		evid_code[9] = 1
		
		outfile.write("%s\t%s\t%s\n" % (id,miRNA_id,"".join(map(str, evid_code))))
	outfile.close()

def parse_pictar(infile_name,outfile_name="TargetDatabases/pictar.txt"):
	infile = open(infile_name)
	outfile = open(outfile_name,"w")
	for line in infile:
		line = line.strip().split("\t")
		id = line[8].split(";")[0].replace("ID=","")
		mult_ids = line[-3].replace("|PicTar","").split("|")
		for miRNA_id in mult_ids:
			if len(miRNA_id) == 0: continue
			if miRNA_id.startswith("miR"):
				base_mirna = miRNA_id
				miRNA_id = "mmu-%s" % (miRNA_id)
			else:
				if miRNA_id.startswith("-"):
					miRNA_id = "mmu-miR-%s%s" % (base_mirna,miRNA_id)
				elif miRNA_id.split("-")[0][0].isdigit():
					miRNA_id = "mmu-miR-%s" % (miRNA_id)
				else:
					if base_mirna[-1].isdigit():
						miRNA_id = "mmu-%s%s" % (base_mirna,miRNA_id)
					else:
						miRNA_id = "mmu-%s%s" % (base_mirna[0:-1],miRNA_id)			
			score1 = line[-3]
			evid_code = 50*[0]
			evid_code[5] = 1
			evid_code[9] = 1 
			outfile.write("%s\t%s\t%s\n" % (id,miRNA_id,"".join(map(str, evid_code))))
	outfile.close()

	
def get_code_experiment(exp):
	code = "ex"
	exp = exp.lower().replace("\"","").replace(";","").replace("other","")
	if exp in ["microarray","micro-array","sequencing","proteomics","chip-seq","chipseq","degradome","quantitative proteomic approach","ngs"]:
		code += "h"
		#The code below is going to cause problems... Just link every experiment to a unique code
		code += exp[0]
	elif exp in ["reporter genes","reporter-genes","qpcr","q-pcr","western-blot","western blot","northern-blot","biotin","clip","clash","clap/clash",
				"luciferase reporter assay","reporter assay","qrt-pcr","northern blot","in situ hybridization","immunoblot","\"immunoblot","branched dna probe assay",
				"immunohistochemistry","elisa","immunofluorescence","immunoprecipitation","immunoprecipitaion","gfp reporter assay"]:
		code += "l"
		#The code below is going to cause problems... Just link every experiment to a unique code
		code += exp[0]
	else:
		code += ""
	return code
	
def parse_mirtarbase(infile_name,outfile_name="TargetDatabases/mirtarbase.txt"):
	infile = open(infile_name)
	outfile = open(outfile_name,"w")
	for line in infile:
		if line.startswith("miRTarBase"): continue
		line = line.strip().split("\t")
		
		id = line[4]
		miRNA_id = line[1]
		evidences = line[6].split("//")
		for evidence in evidences:
			evid_code = 50*[0]
			evid_code[1] = 1
			evid_code[8] = 1
			code = get_code_experiment(evidence)
			if code.startswith("exl"):
				evid_code[11] = 1
			if code.startswith("exh"):
				evid_code[12] = 1
			outfile.write("%s\t%s\t%s\n" % (id,miRNA_id,"".join(map(str, evid_code))))
	outfile.close()

def parse_mirecords(infile_name,outfile_name="TargetDatabases/mirecords.txt"):
	infile = open(infile_name)
	outfile = open(outfile_name,"w")
	for line in infile:
		if line.startswith("Pubmed_id"): continue
		line = line.strip().split("\t")
		id = line[3]
		miRNA_id = line[6].replace("[","").replace("]","")
		evid_code = 50*[0]
		evid_code[3] = 1
		evid_code[8] = 1
		evid_code[11] = 1
		outfile.write("%s\t%s\t%s\n" % (id,miRNA_id,"".join(map(str, evid_code))))
	outfile.close()

#######################################################
# 					/end_block_code					  #
#######################################################
	
parse_connection_table("TargetDatabases/connectionTable.txt")
parse_mirwalk("TargetDatabases/miRWalk.zip")
parse_reptar("TargetDatabases/mouse_pred-RepTar.zip")
parse_pictar("TargetDatabases/dorinaPictar.tsv")
parse_mirtarbase("TargetDatabases/mmu_miRTarBase.txt")
parse_mirecords("TargetDatabases/miRecords_version4.txt")
parse_targetscan("TargetDatabases/TargetScan_mmu.txt")