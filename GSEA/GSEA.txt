db,mirecords,MirTarBase,miRWalk,mRNA,PicTar,RepTar,TargetScan,Two,Three,Four,Penalized
b cell receptor signaling pathway,NA,0.014050776,0.013607488,7.83E-04,0.033271989,0.033927191,NA,NA,0.03333999,0.008003268,0.027699735
foxo signaling pathway,NA,0.004169888,NA,6.82E-04,1.54E-08,NA,0.001478979,NA,2.50E-04,0.001669867,1.12E-07
positive regulation of b cell proliferation,NA,NA,0.016697455,NA,NA,NA,NA,NA,NA,NA,0.007524329
b cell activation involved in immune response,NA,NA,NA,0.011081657,0.031154941,NA,NA,NA,NA,NA,0.025922542
b cell proliferation,NA,0.012158256,0.001190596,NA,NA,NA,NA,NA,NA,NA,NA
regulation of b cell proliferation,NA,0.035494747,0.036636152,NA,NA,NA,NA,NA,NA,NA,0.020136855
immature b cell differentiation,NA,0.00170792,NA,NA,NA,NA,NA,NA,NA,NA,NA
b cell apoptotic process,NA,0.008450219,NA,NA,NA,NA,0.013162174,NA,NA,NA,NA
b cell homeostasis,2.47E-06,NA,0.003152333,NA,NA,NA,NA,NA,NA,NA,NA
b cell differentiation,NA,NA,0.00297883,0.007025494,7.47E-06,NA,0.008523159,NA,NA,NA,6.70E-06
