def get_terms(infile,must_contain=["b cell","b-cell","bcell","foxo"]):
	terms = []
	global bterms
	for line in infile:
		orig_line = line
		line = line.strip().lower()
		term = line.split("\t")[2]
		
		for must in must_contain:
			if must in term:
				new_terms = orig_line.split("\t")[11].replace("[","").replace("]","").split(",")
				bterms.extend(new_terms)
				terms.append(term)
	return list(set(terms))

bterms = []

mirecords = open("cluego/mirecords.txt").readlines()
mirtarbase = open("cluego/mirtarbase.txt").readlines()
mirwalk = open("cluego/mirwalk.txt").readlines()
mrna = open("cluego/mrna.txt").readlines()
pictar = open("cluego/pictar.txt").readlines()
reptar = open("cluego/reptar.txt").readlines()
targetscan = open("cluego/targetscan.txt").readlines()
two = open("cluego/two.txt").readlines()
three = open("cluego/three.txt").readlines()
four = open("cluego/four.txt").readlines()
pen = open("cluego/pen.txt").readlines()

out = open("cluego/needsterms.txt","w")

unique_terms = get_terms(mirecords)
unique_terms.extend(get_terms(mirtarbase))
unique_terms.extend(get_terms(mirwalk))
unique_terms.extend(get_terms(mrna))
unique_terms.extend(get_terms(pictar))
unique_terms.extend(get_terms(reptar))
unique_terms.extend(get_terms(targetscan))
unique_terms.extend(get_terms(two))
unique_terms.extend(get_terms(three))
unique_terms.extend(get_terms(four))

bterms = list(set(bterms))
for term in bterms:
	out.write(term+"\n")
out.close()

def get_pval_term_dict(infile,unique_terms):
	dict = {}
	for line in infile:
		line = line.strip().lower()
		dict[line.split("\t")[2]] = line.split("\t")[8]
	return dict

unique_terms = list(set(unique_terms))
mirecordsDict = get_pval_term_dict(mirecords,unique_terms)
mirtarbaseDict = get_pval_term_dict(mirtarbase,unique_terms)
mirwalkDict = get_pval_term_dict(mirwalk,unique_terms)
mrnaDict = get_pval_term_dict(mrna,unique_terms)
pictarDict = get_pval_term_dict(pictar,unique_terms)
reptarDict = get_pval_term_dict(reptar,unique_terms)
targetscanDict = get_pval_term_dict(targetscan,unique_terms)
twoDict = get_pval_term_dict(two,unique_terms)
threeDict = get_pval_term_dict(three,unique_terms)
fourDict = get_pval_term_dict(four,unique_terms)
penDict = get_pval_term_dict(pen,unique_terms)


def get_pvals(dict,unique_terms,main="",outfile="bcellTerms.txt"):
	outfile = open(outfile,"a")
	outfile.write(main+"\n")
	for term in unique_terms:
		try:
			outfile.write("%s\t%s\n" % (term,dict[term]))
		except:
			outfile.write("%s\t%s\n" % (term,"NA"))
	outfile.write("---------------------\n")
	outfile.close()

get_pvals(mirecordsDict,unique_terms,main="mirecords")
get_pvals(mirtarbaseDict,unique_terms,main="mirtarbase")
get_pvals(mirwalkDict,unique_terms,main="mirwalk")
get_pvals(mrnaDict,unique_terms,main="mrna")
get_pvals(pictarDict,unique_terms,main="pictar")
get_pvals(reptarDict,unique_terms,main="reptar")
get_pvals(targetscanDict,unique_terms,main="targetscan")
get_pvals(twoDict,unique_terms,main="two")
get_pvals(threeDict,unique_terms,main="three")
get_pvals(fourDict,unique_terms,main="four")
get_pvals(penDict,unique_terms,main="pen")